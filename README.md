LibertyCode_Register
====================



Description
-----------

Library contains register components, 
allows to design engine to storage key-value pairs.
Values are named items.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/register ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/register": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Item collection

Item collection allows to register item, 
with unique hash as key.

_Elements_

- ItemCollection
    
    Allows to store items with unique hash associated.

- InstanceCollection

    Extends item collection features. 
    Accept only objects as item.

#### Referential data

Simple array data allows to manage key, value as item hash, pairs.

#### Register

Register allows to design save engine, 
to save items.

_Elements_

- Register
    
    Allows to design items storage engine, 
    to manage items, on specific storage support.
    
- MemoryRegister
    
    Extends register features.
    Uses memory system, as storage support.
    It's composed by:
    -   Referential data for key storage.
    -   Item collection for item storage. 
    -   Object references storage optimized: 
        If 2 keys refer to same item, only one item stored.
    
- TableRegister

    Extends register features.
    Uses specific array, as storage support.

- DefaultTableRegister

    Extends table register features.
    Uses array property, as storage support.
    
- GlobalVarRegister

    Extends table register features.
    Uses global variable array, as storage support.

- MultiRegister
    
    Extends register features. 
    Uses list of registers, as storage support.
    
_Example_

```php
use liberty_code\register\register\model\DefaultRegister;
$register = new DefaultRegister();
...
$register->putItem('key_1', '...'); // Register specified item for key 1
$register->putItem('key_N', '...'); // Register specified item for key N
...
foreach($register->getTabKey() as $key) {
    var_dump($register->getItem($key));
}
/**
 * Show: 
 * item for key 1
 * item for key N
 */
...
```

---


