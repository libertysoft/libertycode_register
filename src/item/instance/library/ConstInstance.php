<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\item\instance\library;



class ConstInstance
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Exception message constants
	const EXCEPT_MSG_INSTANCE_INVALID_FORMAT = 'Following instance "%1$s" invalid! The instance must be an object, not null.';
}