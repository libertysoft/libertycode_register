<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\item\instance\exception;

use liberty_code\register\item\instance\library\ConstInstance;



class InstanceInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $instance
     */
	public function __construct($instance) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstInstance::EXCEPT_MSG_INSTANCE_INVALID_FORMAT,
            mb_strimwidth(strval($instance), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methodes statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified instance has valid format
	 * 
     * @param mixed $instance
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($instance)
    {
		// Init var
		$result = (!is_null($instance)) && (is_object($instance));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($instance) ? serialize($instance) : $instance));
		}
		
		// Return result
		return $result;
    }
	
	
	
}