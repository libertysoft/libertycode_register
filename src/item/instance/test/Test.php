<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\register\item\instance\model\InstanceCollection;



// Init class
class test1
{
	protected $strTest_1 = 'test 1';
	public $strTest_2 = 'test 1';
	
	public function get()
	{
		return $this->strTest_1;
	}
	
	public function set($strTest)
	{
		$this->strTest_1 = $strTest;
	}
};

class test2 extends test1
{
	
};

class test3 extends test1
{
	protected $strTest_1 = 'test 3';
	public $strTest_2 = 'test 3';
};



// Init var
$objInstanceCollection = new InstanceCollection();

$objTest1 = new test1();

$objTest2 = new test2();
$objTest2->set('test 2 upd');
$objTest2->strTest_2 = 'test 2 upd';

$objTest3 = new test3();
$objTest3->set('test 3 upd');
$objTest3->strTest_2 = 'test 3 upd';

$objTest4 = new test3();
$objTest4->set('test 3 upd');
$objTest4->strTest_2 = 'test 3 upd';

$objTest5 = $objTest3;



// Test add items
echo('Test add items: <br />');

$tabInstance = array(
	$objTest1,
	$objTest1,
	$objTest2,
	$objTest3,
	$objTest4,
	$objTest5,
	true,
	1,
	2.2,
	[],
	null
);

$tabKey = array();
foreach($tabInstance as $instance)
{
	try{
		$strKey = $objInstanceCollection->addItem($instance);
		$tabKey[] = $strKey;
		
		echo('Key: <pre>');var_dump($strKey);echo('</pre>');
		echo('Value: <pre>');var_dump($instance);echo('</pre>');
		echo('Value from collection: <pre>');var_dump($objInstanceCollection->beanGetData($strKey));echo('</pre>');
		echo('<br />');
	} catch(\Exception $e) {
		echo($e->getMessage());
		echo('<br />');
	}
}

echo('<br /><br /><br />');



// Test run items
echo('Test run items: <br />');

$intCpt = 0;
foreach($objInstanceCollection as $strKey => $instance)
{
	echo('Key: <pre>');var_dump($strKey);echo('</pre>');
	echo('Key from add: <pre>');var_dump($tabKey[$intCpt]);echo('</pre>');
	echo('Value: <pre>');print_r($instance);echo('</pre>');
	echo('<br />');
	
	$intCpt++;
}

echo('<br /><br /><br />');


