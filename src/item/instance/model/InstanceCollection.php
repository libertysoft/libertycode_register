<?php
/**
 * Description :
 * This class allows to define object items collection class.
 * key => value: item (object).
 * It registers value in collection with key like unique hash.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\item\instance\model;

use liberty_code\register\item\model\ItemCollection;

use liberty_code\register\item\instance\exception\InstanceInvalidFormatException;



class InstanceCollection extends ItemCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			InstanceInvalidFormatException::setCheck($value);
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}
	
	
	
}