<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
// use liberty_code\register\item\library\ConstItem;
use liberty_code\register\item\model\ItemCollection;



// Init class
class test1
{
	protected $strTest_1 = 'test 1';
	public $strTest_2 = 'test 1';
	
	public function get()
	{
		return $this->strTest_1;
	}
	
	public function set($strTest)
	{
		$this->strTest_1 = $strTest;
	}
};



// Init var
$objItemCollection = new ItemCollection();
$objTest1 = new test1();



// Test add items
echo('Test add items: <br />');

$tabItem = array(
	$objTest1,
	true,
	1,
	2.2,
	[
		'Test', 
		$objTest1, 
		false
	],
	[
		'key 1' => 'Test 3',
		'key 2' => new test1(),
		'key 3' => 3.3,
		true
	],
	null,
	[]
);

$tabKey = array();
foreach($tabItem as $item)
{
	try{
		// ConstItem::CONF_ITEM_HASH_PREFIX;
		$strKey = $objItemCollection->addItem($item);
		$tabKey[] = $strKey;
		
		echo('Key: <pre>');var_dump($strKey);echo('</pre>');
		echo('Value: <pre>');var_dump($item);echo('</pre>');
		echo('Value from collection: <pre>');var_dump($objItemCollection->beanGetData($strKey));echo('</pre>');
		echo('<br />');
	} catch(\Exception $e) {
		echo($e->getMessage());
		echo('<br />');
	}
}

echo('<br /><br /><br />');



// Test run items
echo('Test run items: <br />');

$intCpt = 0;
foreach($objItemCollection as $strKey => $item)
{
	echo('Key: <pre>');var_dump($strKey);echo('</pre>');
	echo('Key from add: <pre>');var_dump($tabKey[$intCpt]);echo('</pre>');
	echo('Value: <pre>');print_r($item);echo('</pre>');
	echo('<br />');
	
	$intCpt++;
}

echo('<br /><br /><br />');



// Test get key from item
$tabItem = array(
	$objTest1, // Found
	new test1(), // Not found
	$objTest1, // Found
	true, // Not found
	1,  // Not found
	2.2, // Not found
	[],  // Not found
	null, // Not found
	3 // Not found
);

echo('Test get key from item: <br />');

foreach($tabItem as $item)
{
	$strKey = $objItemCollection->getStrKey($item);

	echo('Item: <pre>');var_dump($item);echo('</pre>');
	echo('Key: <pre>');var_dump($strKey);echo('</pre>');
	echo('<br />');
}

echo('<br /><br /><br />');


