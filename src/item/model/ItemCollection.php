<?php
/**
 * Description :
 * This class allows to define default items collection class.
 * key => value: item.
 * It registers value in collection with key like unique hash.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\item\model;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\register\item\library\ConstItem;
use liberty_code\register\item\exception\ItemInvalidFormatException;



class ItemCollection extends DefaultBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Associative array (key: object hash => value: collection key).
	 * Allows to search faster, a key from an object.
	 * @var array
	 */
	protected $tabObjHash;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function __construct($tabData = array())
	{
		// Init var
		$this->tabObjHash = array();

		// Call parent constructor
		parent::__construct($tabData);
	}





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			ItemInvalidFormatException::setCheck($value);
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





	// Methods getters
	// ******************************************************************************

	/**
	 * Get string key (Hash), from specified item if possible, from cache.
	 *
	 * @param mixed $item
	 * @return null|string
	 */
	protected function getStrKeyFromObjHash($item)
	{
		// Init var
		$result = null;

		// Check item is object
		if(is_object($item))
		{
			// Get object hash and check if key found in cache
			$strHash = spl_object_hash($item);
			if(array_key_exists($strHash, $this->tabObjHash))
			{
				// Get object key
				$strKey = $this->tabObjHash[$strHash];

				// Check if cache valid
				$boolMemoryValid = false;
				if($this->beanDataExists($strKey))
				{
					$collectionItem = $this->beanGetData($strKey);
					$boolMemoryValid =
						is_object($collectionItem) &&
						// ($item === $collectionItem); // Check reference equals (By object comparison)
						($strHash == spl_object_hash($collectionItem)); // Check reference equals (By hash object comparison)
				}

				// Register key in result if cache valid
				if($boolMemoryValid)
				{
					$result = $strKey;
				}
				// Else: remove from cache
				else
				{
					unset($this->tabObjHash[$strHash]);
				}
			}
		}

		// return result
		return $result;
	}



	/**
	 * Get string key (Hash), from specified item if possible.
	 *
	 * @param mixed $item
	 * @param boolean $boolUseMemory = false
	 * @return null|string
	 */
	public function getStrKey($item, $boolUseMemory = true)
	{
		// Init var
		$result = null;
		$boolUseMemory = (is_bool($boolUseMemory) ? $boolUseMemory : true);

		// Try to get from cache, if required
		if($boolUseMemory)
		{
			$result = $this->getStrKeyFromObjHash($item);
		}

		// Check if search in this collection required
		if(is_null($result))
		{
			// Check item is object
			if(is_object($item))
			{
				// Get object hash
				$strHash = spl_object_hash($item);

				// Run all items in collection
				foreach($this as $strKey => $collectionItem)
				{
					// Check if object provided, already exists in collection
					if(
						is_object($collectionItem) &&
						// ($item === $collectionItem) // Check reference equals (By object comparison)
						($strHash == spl_object_hash($collectionItem)) // Check reference equals (By hash object comparison)
					)
					{
						// Get key (object hash)
						$result = $strKey;

						break;
					}
				}

				// Store result in memory if required
				if(
					$boolUseMemory && // Check memory required
					(!is_null($result)) // Check key found
				)
				{
					$this->tabObjHash[$strHash] = $result;
				}
			}
		}

		// return result
		return $result;
	}




	
	// Methods setters
	// ******************************************************************************

	/**
	 * Add item and return hash.
	 * 
	 * @param mixed $item
	 * @return string
	 * @throws ItemInvalidFormatException
     */
	public function addItem($item)
	{
		// Init var
		$strHash = static::getStrHash();
		
		// Register instance
		$this->beanAdd($strHash, $item);
		
		// return result
		return $strHash;
	}
	
	
	
	
	
	// Methods statics
    // ******************************************************************************
	
    /**
     * Get string hash
	 *
	 * @return string
     */
    public static function getStrHash()
    {
		// Return result
        return ConstItem::CONF_ITEM_HASH_PREFIX . ToolBoxHash::getStrHashUniq();
    }
	
	
	
}