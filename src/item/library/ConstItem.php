<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\item\library;



class ConstItem
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Configuration
	const CONF_ITEM_HASH_PREFIX = 'item_';
	
	
	
	// Exception message constants
	const EXCEPT_MSG_ITEM_INVALID_FORMAT = 'Following item "%1$s" invalid! The item cannot be null.';
}