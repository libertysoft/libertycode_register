<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\item\exception;

use liberty_code\register\item\library\ConstItem;



class ItemInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $item
     */
	public function __construct($item) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstItem::EXCEPT_MSG_ITEM_INVALID_FORMAT,
            mb_strimwidth(strval($item), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified item has valid format
	 * 
     * @param mixed $item
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($item)
    {
		// Init var
		$result = (!is_null($item));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($item);
		}
		
		// Return result
		return $result;
    }
	
	
	
}