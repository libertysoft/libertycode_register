<?php
/**
 * Description :
 * This class allows to define default register class.
 * Can be consider is base of all register types.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\register\register\api\RegisterInterface;



abstract class DefaultRegister extends FixBean implements RegisterInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	/**
	 * @inheritdoc
     */
    public function hydrateItem(array $tabItem = array(), $boolOverwrite = false, $boolClear = true)
    {
        // Init var
        $result = true;
        $boolOverwrite = (is_bool($boolOverwrite) ? $boolOverwrite : false);
        $boolClear = (is_bool($boolClear) ? $boolClear : true);

		// Reset items, if required
		if($boolClear)
		{
            $result = $this->removeItemAll() && $result;
		}
		
		// Run each item
        foreach($tabItem as $strKey => $item)
		{
			// Set item
            $result =
                (
                    $boolOverwrite ?
                        $this->putItem($strKey, $item) :
                        $this->addItem($strKey, $item)
                ) &&
                $result;
		}

        // Return result
        return $result;
    }



	
	
	// Methods check
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function checkItemExists($strKey)
	{
		return (!is_null($this->getItem($strKey)));
	}
	
	
	
	
	
	// Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabItem(array $tabConfig = null)
    {
        // Init var
        $result = array();
        $tabKey = $this->getTabKey($tabConfig);

        // Run each item
        foreach($tabKey as $strKey)
        {
            // Set item
            $result[$strKey] = $this->getItem($strKey);
        }

        // Return result
        return $result;
    }


	
	
	
	// Methods setters
    // ******************************************************************************
	
	/**
     * Configuration array format:
     * @see addItem() configuration array format.
     *
	 * @inheritdoc
	 */
	public function setItem($strKey, $item, array $tabConfig = null)
	{
        // Ini var
        $result =
            $this->removeItem($strKey) &&
            $this->addItem($strKey, $item, $tabConfig);

        // Return result
        return $result;
	}
	
	
	
	/**
     * Configuration array format:
     * @see addItem() configuration array format
     * OR
     * @see setItem() configuration array format.
     *
	 * @inheritdoc
	 */
	public function putItem($strKey, $item, array $tabConfig = null)
	{
        // Ini var
        $result = (
            $this->checkItemExists($strKey) ?
                $this->setItem($strKey, $item, $tabConfig) :
                $this->addItem($strKey, $item, $tabConfig)
        );

        // Return result
        return $result;
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function removeItemAll(array $tabConfig = null)
	{
		// Ini var
        $result = true;
		$tabKey = $this->getTabKey($tabConfig);

		// Run each item
		foreach($tabKey as $strKey)
		{
		    // Remove item
            $result = $this->removeItem($strKey) && $result;
		}

        // Return result
        return $result;
	}
	
	
	
}