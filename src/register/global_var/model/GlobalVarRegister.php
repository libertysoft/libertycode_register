<?php
/**
 * Description :
 * This class allows to define global variable register class.
 * Global variable register is table register,
 * using global variable array ($GLOBALS), as storage support.
 *
 * Global variable table register uses the following specified configuration:
 * [
 *     Table register configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\global_var\model;

use liberty_code\register\register\table\model\TableRegister;

use liberty_code\register\register\global_var\library\ConstGlobalVarRegister;



class GlobalVarRegister extends TableRegister
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkItemDataIsScoped($strKey, array $itemData)
    {
        // Return result
        return (
            parent::checkItemDataIsScoped($strKey, $itemData) &&
            (!in_array($strKey, ConstGlobalVarRegister::CONFIG_EXCLUDE_KEY))
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function &getUseTabItemData()
    {
        return $GLOBALS;
    }


	
}