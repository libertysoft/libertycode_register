<?php
/**
 * Description :
 * This class allows to define default table register class.
 * Default table register is table register,
 * using array property, as storage support.
 *
 * Default table register uses the following specified configuration:
 * [
 *     Table register configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\table\model;

use liberty_code\register\register\table\model\TableRegister;



class DefaultTableRegister extends TableRegister
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Array of items
     * @var array
     */
    protected $tabItem;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(
        array $tabConfig = null,
        array $tabItem = null
    )
    {
        // Init var
        $this->tabItem = array();

        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $tabItem
        );
    }





	// Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function &getUseTabItemData()
    {
        return $this->tabItem;
    }
	
	
	
}