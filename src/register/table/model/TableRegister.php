<?php
/**
 * Description :
 * This class allows to define table register class.
 * Table register uses specific array, as storage support.
 * Can be consider is base of all table register types.
 *
 * Items are stored in array like:
 * [
 *     item(required): mixed item,
 *     expire_timeout_datetime(optional): DateTime object
 * ]
 *
 * Table register uses the following specified configuration:
 * [
 *     timezone_name(optional: got @see date_default_timezone_get() if not found):
 *         "string timezone name, used to evaluate item expiration datetime",
 *
 *     format_data_require(optional: got true if not found): true / false
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\table\model;

use liberty_code\register\register\model\DefaultRegister;

use DateTime;
use DateTimeZone;
use liberty_code\register\register\exception\KeyInvalidFormatException;
use liberty_code\register\register\exception\KeyFoundException;
use liberty_code\register\register\exception\KeyNotFoundException;
use liberty_code\register\register\table\library\ConstTableRegister;
use liberty_code\register\register\table\exception\ConfigInvalidFormatException;
use liberty_code\register\register\table\exception\GetExecConfigInvalidFormatException;
use liberty_code\register\register\table\exception\SetExecConfigInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 */
abstract class TableRegister extends DefaultRegister
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     * @param array $tabConfig = null
	 * @param array $tabItem = null
     */
	public function __construct(
	    array $tabConfig = null,
        array $tabItem = null
    )
	{
		// Call parent constructor
		parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

		// Hydrate items, if required
		if(!is_null($tabItem))
		{
			$this->hydrateItem($tabItem, false, true);
		}
	}





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstTableRegister::DATA_KEY_CONFIG))
        {
            $this->__beanTabData[ConstTableRegister::DATA_KEY_CONFIG] = array();
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
        // Init var
        $tabKey = array(
            ConstTableRegister::DATA_KEY_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
	}



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstTableRegister::DATA_KEY_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}





    // Methods check
    // ******************************************************************************

    /**
     * Check if format data required.
     *
     * @return boolean
     */
    public function checkFormatDataRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!array_key_exists(ConstTableRegister::TAB_CONFIG_KEY_FORMAT_DATA_REQUIRE, $tabConfig)) ||
            (intval($tabConfig[ConstTableRegister::TAB_CONFIG_KEY_FORMAT_DATA_REQUIRE]) !== 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check if specified item data is scoped.
     * Overwrite it to implement specific feature.
     *
     * Item data array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param string $strKey
     * @param array $itemData
     * @return boolean
     */
    protected function checkItemDataIsScoped($strKey, array $itemData)
    {
        // Return result
        return true;
    }



    /**
     * Check if specified item data is valid.
     * Overwrite it to implement specific feature.
     *
     * Item data array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param string $strKey
     * @param array $itemData
     * @return boolean
     */
    protected function checkItemDataValid($strKey, array $itemData)
    {
        // Init var
        $objTz = new DateTimeZone($this->getStrTimezoneName());
        $objNowDt = (new DateTime())->setTimezone($objTz);
        $objExpireTimeoutDt = $this->getObjExpireTimeoutDtFromItemData($itemData);
        $result = (
            // Check expiration
            (
                is_null($objExpireTimeoutDt) ||
                ($objExpireTimeoutDt > $objNowDt)
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkItemExists($strKey)
    {
        // Return result
        return (!is_null($this->getItemDataFromKey($strKey)));
    }





	// Methods getters
    // ******************************************************************************

    /**
     * Get timezone name,
     * used to evaluate item expiration datetime.
     *
     * @return string
     */
    public function getStrTimezoneName()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstTableRegister::TAB_CONFIG_KEY_TIMEZONE_NAME, $tabConfig) ?
                $tabConfig[ConstTableRegister::TAB_CONFIG_KEY_TIMEZONE_NAME] :
                date_default_timezone_get()
        );

        // Return result
        return $result;
    }



    /**
     * Get items data array reference,
     * to use on features.
     *
     * Items data array format:
     * [
     *     // Item data 1
     *     [
     *         @see TableRegister stored item array format
     *     ],
     *     ...,
     *     // Item data N
     *     [...]
     * ]
     *
     * @return array
     */
    abstract protected function &getUseTabItemData();



    /**
     * Get item data,
     * from specified array data.
     * Overwrite it to implement specific feature.
     *
     * Return array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param string $strKey
     * @param mixed $data
     * @return null|array
     */
    protected function getItemDataFromData($strKey, $data)
    {
        // Return result
        return (
            (
                is_string($strKey) &&
                is_array($data) &&
                array_key_exists(ConstTableRegister::TAB_ITEM_DATA_CONFIG_KEY_ITEM, $data)
            ) ?
                $data :
                (
                    $this->checkFormatDataRequired() ?
                        array(ConstTableRegister::TAB_ITEM_DATA_CONFIG_KEY_ITEM => $data) :
                        null
                )
        );
    }



    /**
     * Get array data,
     * from specified item data.
     * Overwrite it to implement specific feature.
     *
     * Item data array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param string $strKey
     * @param array $itemData
     * @return mixed
     */
    protected function getDataFromItemData($strKey, array $itemData)
    {
        // Return result
        return $itemData;
    }



    /**
     * Get item data,
     * from specified key.
     *
     * Return array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param string $strKey
     * @return null|array
     * @throws KeyInvalidFormatException
     */
    protected function getItemDataFromKey($strKey)
    {
        // Check arguments
        $strKey = (is_int($strKey) ? strval($strKey) : $strKey);
        KeyInvalidFormatException::setCheck($strKey);

        // Init var
        $tabItemData = &$this->getUseTabItemData();
        $itemData = (
            array_key_exists($strKey, $tabItemData) ?
                $this->getItemDataFromData($strKey, $tabItemData[$strKey]) :
                null
        );
        $result = null;

        // Get result, if required (scoped, valid)
        if((!is_null($itemData)) && $this->checkItemDataIsScoped($strKey, $itemData))
        {
            $result = ($this->checkItemDataValid($strKey, $itemData) ? $itemData : null);

            // Remove item data, if required (scoped, not valid)
            if(is_null($result))
            {
                unset($tabItemData[$strKey]);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get item data,
     * from specified item.
     * Overwrite it to implement specific feature.
     *
     * Configuration array format:
     * Set execution configuration can be provided.
     * [
     *     expire_timeout(optional): DateTime object OR integer additional seconds
     * ]
     *
     * Return array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param mixed $item
     * @param array $tabConfig = null
     * @return array
     * @throws SetExecConfigInvalidFormatException
     */
    protected function getItemDataFromItem($item, array $tabConfig = null)
    {
        // Check configuration
        SetExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = array(
            ConstTableRegister::TAB_ITEM_DATA_CONFIG_KEY_ITEM => $item
        );

        // Set expire timout datetime, if required
        $objTz = new DateTimeZone($this->getStrTimezoneName());
        $objExpireTimeoutDt = (
            isset($tabConfig[ConstTableRegister::TAB_SET_EXEC_CONFIG_KEY_EXPIRE_TIMEOUT]) ?
                // Set specific time
                (
                    (($expireTimeout = $tabConfig[ConstTableRegister::TAB_SET_EXEC_CONFIG_KEY_EXPIRE_TIMEOUT]) instanceof DateTime) ?
                        $expireTimeout
                            ->setTimezone($objTz) :
                        (new DateTime())
                            ->setTimestamp(time() + $expireTimeout)
                            ->setTimezone($objTz)
                ) :
                null
        );
        if(!is_null($objExpireTimeoutDt))
        {
            $result[ConstTableRegister::TAB_ITEM_DATA_CONFIG_KEY_EXPIRE_TIMEOUT_DATETIME] = $objExpireTimeoutDt;
        }

        // Return result
        return $result;
    }



    /**
     * Get item,
     * from specified item data.
     *
     * Item data array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param array $itemData
     * @return mixed
     */
    protected function getItemFromItemData(array $itemData)
    {
        // Return result
        return $itemData[ConstTableRegister::TAB_ITEM_DATA_CONFIG_KEY_ITEM];
    }



    /**
     * Get expiration timeout datetime,
     * from specified item data.
     *
     * Item data array format:
     * @see getUseTabItemData() items data array format, for one item data.
     *
     * @param array $itemData
     * @return null|DateTime
     */
    protected function getObjExpireTimeoutDtFromItemData(array $itemData)
    {
        $objTz = new DateTimeZone($this->getStrTimezoneName());
        /** @var DateTime $objDt */
        $objDt = (
            array_key_exists(ConstTableRegister::TAB_ITEM_DATA_CONFIG_KEY_EXPIRE_TIMEOUT_DATETIME, $itemData) ?
                $itemData[ConstTableRegister::TAB_ITEM_DATA_CONFIG_KEY_EXPIRE_TIMEOUT_DATETIME] :
                null
        );
        $result = ((!is_null($objDt)) ? $objDt->setTimezone($objTz) : null);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getItem($strKey)
    {
        // Return result
        return (
            (!is_null($itemData = $this->getItemDataFromKey($strKey))) ?
                $this->getItemFromItemData($itemData) :
                null
        );
    }



    /**
     * Get expiration timeout datetime,
     * from specified key.
     *
     * @param string $strKey
     * @return null|DateTime
     */
    public function getObjExpireTimeoutDt($strKey)
    {
        // Return result
        return (
            (!is_null($itemData = $this->getItemDataFromKey($strKey))) ?
                $this->getObjExpireTimeoutDtFromItemData($itemData) :
                null
        );
    }



	/**
     * Configuration array format:
     * Get execution configuration can be provided:
     * [
     *     select_key_regexp_pattern(optional): "string REGEXP pattern",
     *
     *     include_key(optional): [
     *         "string key 1",
     *         ...,
     *         "string key N"
     *     ],
     *
     *     exclude_key(optional): [
     *         "string key 1",
     *         ...,
     *         "string key N"
     *     ]
     * ]
     *
	 * @inheritdoc
     * @throws GetExecConfigInvalidFormatException
	 */
	public function getTabKey(array $tabConfig = null)
	{
        // Check arguments
        GetExecConfigInvalidFormatException::setCheck($tabConfig);

        // Ini var
        $tabItemData = &$this->getUseTabItemData();
        $strKeyPattern = (
            isset($tabConfig[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_SELECT_KEY_REGEXP_PATTERN]) ?
                $tabConfig[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_SELECT_KEY_REGEXP_PATTERN] :
                null
        );
        $tabIncludeKey = (
            isset($tabConfig[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_INCLUDE_KEY]) ?
                $tabConfig[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_INCLUDE_KEY] :
                null
        );
        $tabExcludeKey = (
            isset($tabConfig[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_EXCLUDE_KEY]) ?
                $tabConfig[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_EXCLUDE_KEY] :
                null
        );
        $result = array_filter(
            ((!is_null($tabIncludeKey)) ? $tabIncludeKey : array_keys($tabItemData)),
            function($strKey) use ($strKeyPattern, $tabIncludeKey, $tabExcludeKey) {
                return (
                    (
                        is_null($strKeyPattern) ||
                        (preg_match($strKeyPattern, $strKey) === 1)
                    ) &&
                    (
                        is_null($tabIncludeKey) ||
                        in_array($strKey, $tabIncludeKey)
                    ) &&
                    (
                        is_null($tabExcludeKey) ||
                        (!in_array($strKey, $tabExcludeKey))
                    ) &&
                    (!is_null($this->getItemDataFromKey($strKey)))
                );
            }
        );

		// Return result
		return $result;
	}





	// Methods setters
    // ******************************************************************************

	/**
     * Configuration array format:
     * Set execution configuration can be provided:
     * @see getItemDataFromItem() configuration array format.
     *
	 * @inheritdoc
	 * @throws KeyFoundException
	 */
	public function addItem($strKey, $item, array $tabConfig = null)
	{
        // Ini var
        $result = true;

		// Check not exists
		if(!$this->checkItemExists($strKey))
		{
		    // Use specific array of items data
            $tabItemData = &$this->getUseTabItemData();

            // Set item data
            $tabItemData[$strKey] = $this->getDataFromItemData(
                $strKey,
                $this->getItemDataFromItem($item, $tabConfig)
            );
		}
		// Else throw exception
		else
		{
			throw new KeyFoundException($strKey);
		}

        // Return result
        return $result;
	}



    /**
     * Configuration array format:
     * Set xecution configuration can be provided:
     * @see getItemDataFromItem() configuration array format.
     *
     * @inheritdoc
     * @throws KeyNotFoundException
     */
    public function setItem($strKey, $item, array $tabConfig = null)
    {
        // Ini var
        $result = true;

        // Check not exists
        if($this->checkItemExists($strKey))
        {
            // Use specific array of items data
            $tabItemData = &$this->getUseTabItemData();

            // Set item data
            $tabItemData[$strKey] = $this->getDataFromItemData(
                $strKey,
                $this->getItemDataFromItem($item, $tabConfig)
            );
        }
        // Else throw exception
        else
        {
            throw new KeyNotFoundException($strKey);
        }

        // Return result
        return $result;
    }

	
	
	/**
	 * @inheritdoc
	 * @throws KeyNotFoundException
	 */
	public function removeItem($strKey)
	{
        // Ini var
        $result = true;

		// Check exists
		if($this->checkItemExists($strKey))
		{
            // Use specific array of items data
            $tabItemData = &$this->getUseTabItemData();

			// Remove item data
            unset($tabItemData[$strKey]);
		}
		// Else throw exception
		else
		{
			throw new KeyNotFoundException($strKey);
		}

        // Return result
        return $result;
	}
	
	
	
}