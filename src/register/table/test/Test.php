<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\register\register\table\model\DefaultTableRegister;



// Init var
$objRegister = new DefaultTableRegister();



// Test set config
$tabConfig = array(
    'set_timezone_name' => 'test',
    'format_data_require' => 'test'
);
try{
    $objRegister->setTabConfig($tabConfig);
} catch(\Exception $e) {
    echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
    echo('<br />');
}
echo('Get config: <pre>');var_dump($objRegister->getTabConfig());echo('</pre>');
echo('Get config timezone: <pre>');var_dump($objRegister->getStrTimezoneName());echo('</pre>');
echo('Get config format data required option: <pre>');var_dump($objRegister->checkFormatDataRequired());echo('</pre>');

$tabConfig = array(
    'set_timezone_name' => 'UTC',
    'format_data_require' => false
);
$objRegister->setTabConfig($tabConfig);
echo('Get config: <pre>');var_dump($objRegister->getTabConfig());echo('</pre>');
echo('Get config timezone: <pre>');var_dump($objRegister->getStrTimezoneName());echo('</pre>');
echo('Get config format data required option: <pre>');var_dump($objRegister->checkFormatDataRequired());echo('</pre>');

echo('<br /><br /><br />');



// Test add
$test1 = 'Test 1';
$test3 = true;
$tabData = array(
    ['key_1_1', $test1, ['expire_timeout' => 3000]], // Ok
    ['key_1_1', 'Test 1 duplicate'], // Ko: key found
    ['key_1_2', $test1, ['expire_timeout' => (new DateTime())->add(new DateInterval(sprintf('PT%1$dS', 7)))]], // Ok
    ['key_1_3', $test1, ['expire_timeout' => -3000]], // Ok: never found due to past timeout
    ['key_2', 2], // Ok
    ['key_3', $test3], // Ok
    [
        'key_4',
        array(
            'key_test_get_1' => 'Value test 1',
            'key_test_get_2' => 'Value test 2',
            'key_test_get_N' => 'Value test N',
        )
    ], // Ok
    ['key_5', $test3], // Ok
    ['key_6', 6.7], // Ok
    ['key_7', false], // Ok
    [3, 3.7], // Ok
    ['3', 3.7], // Ko: key found
    ['key_8', null], // Ok
    [true, 'test'], // Ko: bad key format
    ['key_9', 'test', ['expire_timeout' => 'test']] // Ko: bad config format
);

foreach($tabData as $data)
{
    $strKey = $data[0];
    $value = $data[1];
    $tabConfig = (isset($data[2]) ? $data[2] : null);

    echo('Test add "'.$strKey.'": <br />');
    try{
        $objRegister->addItem($strKey, $value, $tabConfig);
        echo('Ok <br />');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test adding (select key regexp pattern): <br />');
foreach($objRegister->getTabKey(array('select_key_regexp_pattern' => '#^key_1.*#')) as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'" timeout: <pre>');var_dump($objRegister->getObjExpireTimeoutDt($strKey));echo('</pre>');
}

echo('Test adding (include keys): <br />');
foreach($objRegister->getTabKey(array('include_key' => ['key_2', 'key_3', 'key_4'])) as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'" timeout: <pre>');var_dump($objRegister->getObjExpireTimeoutDt($strKey));echo('</pre>');
}

echo('Test adding (exclude keys): <br />');
foreach($objRegister->getTabKey(array('exclude_key' => ['key_2', 'key_3', 'key_4'])) as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'" timeout: <pre>');var_dump($objRegister->getObjExpireTimeoutDt($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test check/get
$tabKey = array(
	'key_1_1', // Found
	'key_1_2', // Found
	'key_2', // Found
	'key_3', // Found
	'key_4', // Found
	'key_5', // Found
	'key_6', // Found
	'key_7', // Found
	'key_8', // Found
	'key_9', // Not found
	'test', // Not found
	3, // Ok
    '3', // Ok,
    true // Ko: bad key format
);

foreach($tabKey as $strKey)
{
	echo('Test check, get "'.$strKey.'": <br />');
	try{
		echo('Exists: <pre>');var_dump($objRegister->checkItemExists($strKey));echo('</pre>');
		echo('Get: <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
	} catch(\Exception $e) {
		echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('<br /><br /><br />');



// Test set
$tabData = array(
	[
	    'key_4',
        array(
            'key_test_get_1' => 'Value test 1 updated',
            'key_test_get_2' => 'Value test 2 updated',
            'key_test_get_3' => 'Value test 3',
            'key_test_get_N' => 'Value test N updated',
        )
    ], // Ok
	['key_5', 'Test 5 update', ['expire_timeout' => 3000]], // Ok
	['key_2', null], // Ok
	['test', 'test'], // Ko: Key not found
    [true, 'test'], // Ko: bad key format
    ['key_7', true, ['expire_timeout' => 'test']] // Ko: bad config format
);

foreach($tabData as $data)
{
    $strKey = $data[0];
    $value = $data[1];
    $tabConfig = (isset($data[2]) ? $data[2] : null);

	echo('Test set "'.$strKey.'": <br />');
	try{
		$objRegister->setItem($strKey, $value, $tabConfig);
        echo('Ok <br />');
	} catch(\Exception $e) {
		echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test setting: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'" timeout: <pre>');var_dump($objRegister->getObjExpireTimeoutDt($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test put
$tabData = array(
	['key_7', true, ['expire_timeout' => 3000]], // Ok: update
	['key_9', 9, ['expire_timeout' => 3000]], // Ok: create
	['key_1_1', 'Test 1 updated'], // Ok: update
	[true, 'Test'], // Ko: bad key format
    ['key_7', false, ['expire_timeout' => 'test']] // Ko: bad config format
);

foreach($tabData as $data)
{
    $strKey = $data[0];
    $value = $data[1];
    $tabConfig = (isset($data[2]) ? $data[2] : null);

	echo('Test put "'.$strKey.'": <br />');
	try{
		$objRegister->putItem($strKey, $value, $tabConfig);
        echo('Ok <br />');
	} catch(\Exception $e) {
		echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test putting: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    echo('Get "'.$strKey.'" timeout: <pre>');var_dump($objRegister->getObjExpireTimeoutDt($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test remove
$tabKey = array(
	'key_1_2', // Ok
	'key_2', // Ok
	'key_6', // Ok
	'key_1', // Ko: not found
	'test', // Ko: not found
	3, // Ok
    '3', // Ko: not found
    true // Ko: bad key format
);

foreach($tabKey as $strKey)
{
	echo('Test remove "'.$strKey.'": <br />');
	try{
		$objRegister->removeItem($strKey);
        echo('Ok <br />');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test removing: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test hydrate
$tabItem = array(
	'key_9' => 'Test 9', // Ok: create
	'key_10' => 10 // Ok: create
);
$objRegister->hydrateItem($tabItem);

echo('Test hydrate: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test hydrate flush
$objRegister->hydrateItem();

echo('Test hydrate flush: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('<br /><br /><br />');


