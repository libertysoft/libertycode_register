<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\table\library;



class ConstTableRegister
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_TIMEZONE_NAME = 'set_timezone_name';
    const TAB_CONFIG_KEY_FORMAT_DATA_REQUIRE = 'format_data_require';

    // Get execution configuration
    const TAB_GET_EXEC_CONFIG_KEY_SELECT_KEY_REGEXP_PATTERN = 'select_key_regexp_pattern';
    const TAB_GET_EXEC_CONFIG_KEY_INCLUDE_KEY = 'include_key';
    const TAB_GET_EXEC_CONFIG_KEY_EXCLUDE_KEY = 'exclude_key';

    // Set execution configuration
    const TAB_SET_EXEC_CONFIG_KEY_EXPIRE_TIMEOUT = 'expire_timeout';

    // Item data configuration
    const TAB_ITEM_DATA_CONFIG_KEY_ITEM = 'item';
    const TAB_ITEM_DATA_CONFIG_KEY_EXPIRE_TIMEOUT_DATETIME = 'expire_timeout_datetime';


	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the table register configuration standard.';
    const EXCEPT_MSG_GET_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the table register get execution configuration standard.';
    const EXCEPT_MSG_SET_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the table register set execution configuration standard.';



}