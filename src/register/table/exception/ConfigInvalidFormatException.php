<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\table\exception;

use Exception;

use liberty_code\register\register\table\library\ConstTableRegister;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstTableRegister::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $tabTzNm = timezone_identifiers_list();
        $result =
            // Check valid timezone name
            (
                (!isset($config[ConstTableRegister::TAB_CONFIG_KEY_TIMEZONE_NAME])) ||
                (
                    is_string($config[ConstTableRegister::TAB_CONFIG_KEY_TIMEZONE_NAME]) &&
                    (in_array($config[ConstTableRegister::TAB_CONFIG_KEY_TIMEZONE_NAME], $tabTzNm))
                )
            ) &&

            // Check valid format data required option
            (
                (!isset($config[ConstTableRegister::TAB_CONFIG_KEY_FORMAT_DATA_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstTableRegister::TAB_CONFIG_KEY_FORMAT_DATA_REQUIRE]) ||
                    is_int($config[ConstTableRegister::TAB_CONFIG_KEY_FORMAT_DATA_REQUIRE]) ||
                    (
                        is_string($config[ConstTableRegister::TAB_CONFIG_KEY_FORMAT_DATA_REQUIRE]) &&
                        ctype_digit($config[ConstTableRegister::TAB_CONFIG_KEY_FORMAT_DATA_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}