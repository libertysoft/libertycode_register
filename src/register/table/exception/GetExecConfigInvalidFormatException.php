<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\table\exception;

use Exception;

use liberty_code\register\register\table\library\ConstTableRegister;



class GetExecConfigInvalidFormatException extends Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstTableRegister::EXCEPT_MSG_GET_EXEC_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function($tabStr)
        {
            $result = is_array($tabStr) && (count($tabStr) > 0);

            // Check each column name is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = is_string($strValue) && (trim($strValue) != '');
            }

            return $result;
        };
        
        // Init var
        $result =
            // Check valid select key regexp pattern
            (
                (!isset($config[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_SELECT_KEY_REGEXP_PATTERN])) ||
                (
                    is_string($config[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_SELECT_KEY_REGEXP_PATTERN]) &&
                    (trim($config[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_SELECT_KEY_REGEXP_PATTERN]) !== '')
                )
            ) &&
            
            // Check valid include keys
            (
                (!isset($config[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_INCLUDE_KEY])) ||
                $checkTabStrIsValid($config[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_INCLUDE_KEY])
            ) &&

            // Check valid exclude keys
            (
                (!isset($config[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_EXCLUDE_KEY])) ||
                $checkTabStrIsValid($config[ConstTableRegister::TAB_GET_EXEC_CONFIG_KEY_EXCLUDE_KEY])
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}