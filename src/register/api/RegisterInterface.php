<?php
/**
 * Description :
 * This class allows to describe behavior of register class.
 * Register allows to design items storage engine and
 * implements all actions, to manage items, on specific storage support.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\api;



interface RegisterInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods initialize
    // ******************************************************************************
	
    /**
     * Hydrate specified items.
     * Return true if success, false if an error occurs.
     *
     * Items array format:
     * Associative array of items:
     * Format: key => value: item.
	 *
     * TODO: Remove it, once addTabItem, putTabItem and removeTabItem functions set.
     *
     * @param array $tabItem = array()
	 * @param boolean $boolOverwrite = false
	 * @param boolean $boolClear = true
     * @return boolean
     */
    public function hydrateItem(array $tabItem = array(), $boolOverwrite = false, $boolClear = true);
	
	
	
	
	
	// Methods check
	// ******************************************************************************
	
	/**
	 * Check if the specified item exists.
	 *
     * TODO:
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
	 * @param string $strKey
     * TODO: param array &$tabInfo = null
	 * @return boolean
	 */
	public function checkItemExists($strKey);



    /**
     * TODO:
     * Check if the specified items exist.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array $tabKey
     * @param array &$tabInfo = null
     * @return boolean
     */
    // public function checkTabItemExists(array $tabKey, array &$tabInfo = null);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get specified item.
	 *
     * TODO:
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
	 * @param string $strKey
     * TODO: param null|mixed $default = null
     * TODO: param array &$tabInfo = null
	 * @return null|mixed
	 */
	public function getItem($strKey);



    /**
     * TODO:
     * Get specified items.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * Return array format:
     * Associative array of items:
     * Format: key => value: item.
     *
     * @param array $tabKey
     * @param array &$tabInfo = null
     * @return array
     */
    // public function getTabItem(array $tabKey, array &$tabInfo = null);



    /**
     * Get index array of searched keys.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to customize key selection.
     * Null means no specific execution required and all keys will be selected.
     *
     * TODO:
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array $tabConfig = null
     * TODO: param array &$tabInfo = null
     * @return array
     */
    public function getTabKey(array $tabConfig = null);



    /**
     * Get searched items.
     *
     * Configuration array format:
     * Execution configuration can be provided:
     * @see getTabKey() configuration array format.
     *
     * Return array format:
     * Associative array of items:
     * Format: key => value: item.
     *
     * TODO: Remove it, once getTabItem function above set.
     *
     * @param array $tabConfig = null
     * @return array
     */
    public function getTabItem(array $tabConfig = null);





	// Methods setters
	// ******************************************************************************

	/**
	 * Add specified item.
     * Return true if success, false if an error occurs.
	 *
     * Configuration array format:
     * Execution configuration can be provided,
     * to customize item adding.
     * Null means no specific execution configuration required.
     *
     * TODO:
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
	 * @param string $strKey
	 * @param mixed $item
     * @param array $tabConfig = null
     * TODO: param array &$tabInfo = null
     * @return boolean
     */
	public function addItem($strKey, $item, array $tabConfig = null);



    /**
     * TODO:
     * Add specified items.
     * Return true if success, false if an error occurs.
     *
     * Items array format:
     * Associative array of items:
     * Format: key => value: item.
     *
     * Configuration array format:
     * Execution configuration can be provided:
     * @see addItem() configuration array format.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array $tabItem
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    // public function addTabItem(array $tabItem, array $tabConfig = null, array &$tabInfo = null);



    /**
     * Set (update) specified item.
     * Return true if success, false if an error occurs.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to customize item setting.
     * Null means no specific execution configuration required.
     *
     * TODO:
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param string $strKey
     * @param mixed $item
     * @param array $tabConfig = null
     * TODO: param array &$tabInfo = null
     * @return boolean
     */
    public function setItem($strKey, $item, array $tabConfig = null);



    /**
     * TODO:
     * Set (update) specified items.
     * Return true if success, false if an error occurs.
     *
     * Items array format:
     * Associative array of items:
     * Format: key => value: item.
     *
     * Configuration array format:
     * Execution configuration can be provided:
     * @see setItem() configuration array format.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array $tabItem
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    // public function setTabItem(array $tabItem, array $tabConfig = null, array &$tabInfo = null);



	/**
	 * Put specified item.
     * Update if exists, add else.
     * Return true if success, false if an error occurs.
	 *
     * Configuration array format:
     * Execution configuration can be provided,
     * to customize item putting.
     * Null means no specific execution configuration required.
     *
     * TODO:
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
	 * @param string $strKey
	 * @param mixed $item
     * @param array $tabConfig = null
     * TODO: param array &$tabInfo = null
     * @return boolean
     */
	public function putItem($strKey, $item, array $tabConfig = null);



    /**
     * TODO:
     * Put specified items.
     * Return true if success, false if an error occurs.
     *
     * Items array format:
     * Associative array of items:
     * Format: key => value: item.
     *
     * Configuration array format:
     * Execution configuration can be provided:
     * @see putItem() configuration array format.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array $tabItem
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    // public function putTabItem(array $tabItem, array $tabConfig = null, array &$tabInfo = null);
	
	
	
	/**
	 * Remove specified item.
     * Return true if success, false if an error occurs.
	 *
     * TODO:
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
	 * @param string $strKey
     * TODO: param array &$tabInfo = null
     * @return boolean
     */
	public function removeItem($strKey);



    /**
     * TODO:
     * Remove specified items.
     * Return true if success, false if an error occurs.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array $tabKey
     * @param array &$tabInfo = null
     * @return boolean
     */
    // public function removeTabItem(array $tabKey, array &$tabInfo = null);


	
	/**
	 * Remove search items.
     * Return true if success, false if an error occurs.
     *
     * Configuration array format:
     * Execution configuration can be provided:
     * @see getTabKey() configuration array format.
     *
     * TODO:
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array $tabConfig = null
     * TODO: param array &$tabInfo = null
     * @return boolean
	 */
	public function removeItemAll(array $tabConfig = null);
}