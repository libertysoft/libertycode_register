<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\memory\exception;

use Exception;

use liberty_code\register\referential\model\ReferentialData;
use liberty_code\register\register\memory\library\ConstMemoryRegister;



class ReferentialDataInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $referentialData
     */
	public function __construct($referentialData) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMemoryRegister::EXCEPT_MSG_REFERENTIAL_DATA_FORMAT,
            mb_strimwidth(strval($referentialData), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified referential data has valid format.
	 * 
     * @param mixed $referentialData
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($referentialData)
    {
		// Init var
		$result = ((!is_null($referentialData)) && ($referentialData instanceof ReferentialData));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($referentialData);
		}
		
		// Return result
		return $result;
    }
	
	
	
}