<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\memory\exception;

use Exception;

use liberty_code\register\item\model\ItemCollection;
use liberty_code\register\register\memory\library\ConstMemoryRegister;



class ItemCollectionInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $itemCollection
     */
	public function __construct($itemCollection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMemoryRegister::EXCEPT_MSG_ITEM_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($itemCollection), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified item collection has valid format.
	 * 
     * @param mixed $itemCollection
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($itemCollection)
    {
		// Init var
		$result = ((!is_null($itemCollection)) && ($itemCollection instanceof ItemCollection));

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($itemCollection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}