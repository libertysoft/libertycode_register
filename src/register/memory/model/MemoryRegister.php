<?php
/**
 * Description :
 * This class allows to define memory register class.
 * Memory register uses memory system (item collection and referential data), as storage support.
 *
 * Items are stored like:
 * - Key stored in referential data object
 * - Item stored in item collection object.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\memory\model;

use liberty_code\register\register\model\DefaultRegister;

use liberty_code\register\item\model\ItemCollection;
use liberty_code\register\referential\model\ReferentialData;
use liberty_code\register\register\exception\KeyInvalidFormatException;
use liberty_code\register\register\exception\KeyFoundException;
use liberty_code\register\register\exception\KeyNotFoundException;
use liberty_code\register\register\memory\library\ConstMemoryRegister;
use liberty_code\register\register\memory\exception\ItemCollectionInvalidFormatException;
use liberty_code\register\register\memory\exception\ReferentialDataInvalidFormatException;



/**
 * @method ItemCollection getObjItemCollection() Get object item collection.
 * @method ReferentialData getObjReferentialData() Get object referential.
 * @method void setObjItemCollection(ItemCollection $objItemCollection) Set object item collection.
 * @method void setObjReferentialData(ReferentialData $objReferentialData) Set object referential.
 */
class MemoryRegister extends DefaultRegister
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param ItemCollection $objItemCollection = null
	 * @param ReferentialData $objReferentialData = null
	 * @param array $tabItem = null
     */
	public function __construct(ItemCollection $objItemCollection = null, ReferentialData $objReferentialData = null, array $tabItem = null) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Hydrate item collection if needs
		if(!is_null($objItemCollection))
		{
			$this->setObjItemCollection($objItemCollection);
		}
		
		// Hydrate referential data if needs
		if(!is_null($objReferentialData))
		{
			$this->setObjReferentialData($objReferentialData);
		}
		
		// Hydrate items if needs
		if(!is_null($tabItem))
		{
			$this->hydrateItem($tabItem, false, true);
		}
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstMemoryRegister::DATA_KEY_DEFAULT_ITEM_COLLECTION))
        {
            $this->beanAdd(ConstMemoryRegister::DATA_KEY_DEFAULT_ITEM_COLLECTION, new ItemCollection());
        }

        if(!$this->beanExists(ConstMemoryRegister::DATA_KEY_DEFAULT_REFERENTIAL_DATA))
        {
            $this->beanAdd(ConstMemoryRegister::DATA_KEY_DEFAULT_REFERENTIAL_DATA, new ReferentialData());
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstMemoryRegister::DATA_KEY_DEFAULT_ITEM_COLLECTION,
            ConstMemoryRegister::DATA_KEY_DEFAULT_REFERENTIAL_DATA
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstMemoryRegister::DATA_KEY_DEFAULT_ITEM_COLLECTION:
					ItemCollectionInvalidFormatException::setCheck($value);
					break;

				case ConstMemoryRegister::DATA_KEY_DEFAULT_REFERENTIAL_DATA:
					ReferentialDataInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}
	


	
	
	// Methods getters
    // ******************************************************************************

	/**
	 * @inheritdoc
	 * @throws KeyInvalidFormatException
	 */
	public function getItem($strKey)
	{
		// Check arguments
		KeyInvalidFormatException::setCheck($strKey);
		
		// Ini var
		$result = null;
		$objItemCollection = $this->getObjItemCollection();
		$objReferentialData = $this->getObjReferentialData();
		
		// Get hash if found
		$strHash = null;
		if($objReferentialData->checkValueExists($strKey))
		{
			$strHash = $objReferentialData->getValue($strKey);
		}
		
		// Check hash found
		if(!is_null($strHash))
		{
			// Get item if found
			if($objItemCollection->beanDataExists($strHash))
			{
				$result = $objItemCollection->beanGetData($strHash);
			}
			// Else: Remove hash
			else
			{
				$objReferentialData->removeValue($strKey);
			}
		}
		
		// Return result
		return $result;
	}



    /**
     * @inheritdoc
     */
    public function getTabKey(array $tabConfig = null)
    {
        // Ini var
        $objReferentialData = $this->getObjReferentialData();
        $result = array_keys($objReferentialData->getDataSrc());

        // Return result
        return $result;
    }
	
	


	
	// Methods setters
    // ******************************************************************************

	/**
	 * @inheritdoc
	 * @throws KeyInvalidFormatException
	 * @throws KeyFoundException
	 */
	public function addItem($strKey, $item, array $tabConfig = null)
	{
        // Ini var
        // $result = true;

		// Check not exists
		if(!$this->checkItemExists($strKey))
		{
			// Ini var
			$objItemCollection = $this->getObjItemCollection();
			$objReferentialData = $this->getObjReferentialData();
			
			// Add instance, if required
			$strHash = $objItemCollection->getStrKey($item);
			if(is_null($strHash))
			{
				$strHash = $objItemCollection->addItem($item);
			}
			
			// Add hash in referential
            $result = $objReferentialData->addValue($strKey, $strHash);
		}
		// Else throw exception
		else
		{
			throw new KeyFoundException($strKey);
		}

        // Return result
        return $result;
	}
	
	
	
	/**
	 * @inheritdoc
	 * @throws KeyInvalidFormatException
	 * @throws KeyNotFoundException
	 */
	public function removeItem($strKey)
	{
        // Ini var
        // $result = true;

		// Check exists
		if($this->checkItemExists($strKey))
		{
			// Ini var
			$objItemCollection = $this->getObjItemCollection();
			$objReferentialData = $this->getObjReferentialData();
			
			// Get hash
			$strHash = $objReferentialData->getValue($strKey);
			
			// Remove from referential
            $result = $objReferentialData->removeValue($strKey);
			
			// Remove from item collection, if not used
			if(!$objReferentialData->checkHashExists($strHash))
			{
				$objItemCollection->beanRemoveData($strHash);
			}
		}
		// Else throw exception
		else
		{
			throw new KeyNotFoundException($strKey);
		}

        // Return result
        return $result;
	}
	
	
	
}