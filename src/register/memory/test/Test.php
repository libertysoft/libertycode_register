<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
// use liberty_code\register\item\model\ItemCollection;
use liberty_code\register\item\instance\model\InstanceCollection;
// use liberty_code\register\referential\model\ReferentialData;
use liberty_code\register\register\memory\model\MemoryRegister;



// Init class
class test1
{
	protected $strTest_1 = 'test 1';
	public $strTest_2 = 'test 1';
	
	public function get()
	{
		return $this->strTest_1;
	}
	
	public function set($strTest)
	{
		$this->strTest_1 = $strTest;
	}
};

class test2 extends test1
{
	
};

class test3 extends test1
{
	protected $strTest_1 = 'test 3';
	public $strTest_2 = 'test 3';
};



// Init var
// $objRegister = new MemoryRegister(new ItemCollection(), new ReferentialData());
// $objRegisterInstance = new MemoryRegister(new InstanceCollection(), new ReferentialData());
$objRegister = new MemoryRegister();
$objRegisterInstance = new MemoryRegister(new InstanceCollection());

$objTest1 = new test1();

$objTest2 = new test2();
$objTest2->set('test 2 upd');
$objTest2->strTest_2 = 'test 2 upd';

$objTest3 = new test3();
$objTest3->set('test 3 upd');
$objTest3->strTest_2 = 'test 3 upd';

$objTest4 = new test3();
$objTest4->set('test 3 upd');
$objTest4->strTest_2 = 'test 3 upd';

$objTest5 = $objTest3;



// Test add
echo('Test add: <br />');

$objRegister->addItem('key_1_1', $objTest1);
$objRegister->addItem('key_1_2', $objTest1);
$objRegister->addItem('key_2', $objTest2);
$objRegister->addItem('key_3', $objTest3);
$objRegister->addItem('key_4', $objTest4);
$objRegister->addItem('key_5', $objTest5);
$objRegister->addItem('key_6', 2.7);
$objRegister->addItem('key_7', false);

try{
	$objRegister->addItem('key_8', null);
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

$objRegister->addItem('key_9', 'test Value');

try{
	$objRegister->addItem('key_1_1', $objTest1);
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

echo('Test adding: referential: <br />');
echo('<pre>');print_r($objRegister->getObjReferentialData());echo('</pre>');

echo('<br />');

echo('Test adding: item collection: <br />');
echo('<pre>');print_r($objRegister->getObjItemCollection());echo('</pre>');

echo('<br /><br /><br />');



// Test add (instance)
echo('Test add (instance): <br />');

$objRegisterInstance->addItem('key_1_1', $objTest1);
$objRegisterInstance->addItem('key_1_2', $objTest1);
$objRegisterInstance->addItem('key_2', $objTest2);
$objRegisterInstance->addItem('key_3', $objTest3);
$objRegisterInstance->addItem('key_4', $objTest4);
$objRegisterInstance->addItem('key_5', $objTest5);

try{
	$objRegisterInstance->addItem('key_6', 2.7);
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

try{
	$objRegisterInstance->addItem('key_7', null);
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

echo('Test adding: referential (instance): <br />');
echo('<pre>');print_r($objRegisterInstance->getObjReferentialData());echo('</pre>');

echo('<br />');

echo('Test adding: item collection (instance): <br />');
echo('<pre>');print_r($objRegisterInstance->getObjItemCollection());echo('</pre>');

echo('<br /><br /><br />');



// Test check/get
$tabKey = array(
	'key_1_1', // Found
	'key_1_2', // Found
	'key_2', // Found
	'key_3', // Found
	'key_4', // Found
	'key_5', // Found
	'key_6', // Found | Not found
	'key_7', // Found | Not found
	'key_8', // Not found
	'key_9', // Found | Not found
	'test', // Not found
	3, // Bad key format
);

foreach($tabKey as $strKey)
{
	echo('Test check, get "'.$strKey.'": <br />');
	try{
		echo('Exists: <pre>');var_dump($objRegister->checkItemExists($strKey));echo('</pre>');
		echo('Get: <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
		echo('Exists (instance): <pre>');var_dump($objRegisterInstance->checkItemExists($strKey));echo('</pre>');
		echo('Get (instance): <pre>');var_dump($objRegisterInstance->getItem($strKey));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('<br /><br /><br />');



// Test set
$objTest4 = new test3();
$objTest4->set('test 4 upd');
$objTest4->strTest_2 = 'test 4 upd';

$objTest5 = new test1();
$objTest5->set('test 5 upd');
$objTest5->strTest_2 = 'test 5 upd';

$tabKey = array(
	'key_4' => $objTest4, // Found
	'key_5' => $objTest5, // Found
	'key_1' => $objTest4, // Not found
	'key_2' => null, // Bad value format
	'key_3' => 'test', // Found | Bad value format
	'test' => 'test' // Not found | Not found & bad value format
);

foreach($tabKey as $strKey => $value)
{
	echo('Test set "'.$strKey.'": <br />');
	try{
		$objRegister->setItem($strKey, $value);
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test setting: referential: <br />');
echo('<pre>');print_r($objRegister->getObjReferentialData());echo('</pre>');

echo('<br />');

echo('Test setting: item collection: <br />');
echo('<pre>');print_r($objRegister->getObjItemCollection());echo('</pre>');

echo('<br /><br /><br />');



// Test set (instance)
foreach($tabKey as $strKey => $value)
{
	echo('Test set (instance)"'.$strKey.'": <br />');
	try{
		$objRegisterInstance->setItem($strKey, $value);
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test setting: referential (instance): <br />');
echo('<pre>');print_r($objRegisterInstance->getObjReferentialData());echo('</pre>');

echo('<br />');

echo('Test setting: item collection (instance): <br />');
echo('<pre>');print_r($objRegisterInstance->getObjItemCollection());echo('</pre>');

echo('<br /><br /><br />');



// Test put
$tabKey = array(
	'key_7' => $objTest4, // Update | Create
	'key_9' => $objTest5, // Create
	'key_2' => $objTest3, // Update
	'key_3' => $objTest3, // Update
	'key_1_1' => 'test', // Update | Bad value format
	true => $objTest3 // Bad key format
);

foreach($tabKey as $strKey => $value)
{
	echo('Test put "'.$strKey.'": <br />');
	try{
		$objRegister->putItem($strKey, $value);
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test putting: referential: <br />');
echo('<pre>');print_r($objRegister->getObjReferentialData());echo('</pre>');

echo('<br />');

echo('Test putting: item collection: <br />');
echo('<pre>');print_r($objRegister->getObjItemCollection());echo('</pre>');

echo('<br /><br /><br />');



// Test put (instance)
foreach($tabKey as $strKey => $value)
{
	echo('Test put (instance)"'.$strKey.'": <br />');
	try{
		$objRegisterInstance->putItem($strKey, $value);
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test putting: referential (instance): <br />');
echo('<pre>');print_r($objRegisterInstance->getObjReferentialData());echo('</pre>');

echo('<br />');

echo('Test putting: item collection (instance): <br />');
echo('<pre>');print_r($objRegisterInstance->getObjItemCollection());echo('</pre>');

echo('<br /><br /><br />');



// Test remove
$tabKey = array(
	'key_1_1', // Found
	'key_1_2', // Found
	'key_2', // Found
	'key_6', // Found | Not found
	'key_1', // Not found
	'test', // Not found
	3, // Bad key format
);

foreach($tabKey as $strKey)
{
	echo('Test remove "'.$strKey.'": <br />');
	try{
		$objRegister->removeItem($strKey);
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test removing: referential: <br />');
echo('<pre>');print_r($objRegister->getObjReferentialData());echo('</pre>');

echo('<br />');

echo('Test removing: item collection: <br />');
echo('<pre>');print_r($objRegister->getObjItemCollection());echo('</pre>');

echo('<br /><br /><br />');



// Test remove (instance)
foreach($tabKey as $strKey)
{
	echo('Test remove (instance)"'.$strKey.'": <br />');
	try{
		$objRegisterInstance->removeItem($strKey);
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test removing: referential (instance): <br />');
echo('<pre>');print_r($objRegisterInstance->getObjReferentialData());echo('</pre>');

echo('<br />');

echo('Test removing: item collection (instance): <br />');
echo('<pre>');print_r($objRegisterInstance->getObjItemCollection());echo('</pre>');

echo('<br /><br /><br />');



// Test hydrate
$tabItem = array(
	'key_10' => $objTest4, // Create
	'key_11' => $objTest5 // Create
);
$objRegister->hydrateItem($tabItem);

echo('Test hydrate: referential: <br />');
echo('<pre>');print_r($objRegister->getObjReferentialData());echo('</pre>');

echo('<br />');

echo('Test hydrate: item collection: <br />');
echo('<pre>');print_r($objRegister->getObjItemCollection());echo('</pre>');

echo('<br />');

$objRegister->hydrateItem();

echo('Test hydrate flush: referential: <br />');
echo('<pre>');print_r($objRegister->getObjReferentialData());echo('</pre>');

echo('<br />');

echo('Test hydrate flush: item collection: <br />');
echo('<pre>');print_r($objRegister->getObjItemCollection());echo('</pre>');

echo('<br /><br /><br />');



// Test hydrate (instance)
$objRegisterInstance->hydrateItem($tabItem);

echo('Test hydrate: referential (instance): <br />');
echo('<pre>');print_r($objRegisterInstance->getObjReferentialData());echo('</pre>');

echo('<br />');

echo('Test hydrate: item collection (instance): <br />');
echo('<pre>');print_r($objRegisterInstance->getObjItemCollection());echo('</pre>');

echo('<br />');

$objRegisterInstance->hydrateItem();

echo('Test hydrate flush: referential (instance): <br />');
echo('<pre>');print_r($objRegisterInstance->getObjReferentialData());echo('</pre>');

echo('<br />');

echo('Test hydrate flush: item collection (instance): <br />');
echo('<pre>');print_r($objRegisterInstance->getObjItemCollection());echo('</pre>');

echo('<br /><br /><br />');


