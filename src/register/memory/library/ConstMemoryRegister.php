<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\memory\library;



class ConstMemoryRegister
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Data constants
    const DATA_KEY_DEFAULT_ITEM_COLLECTION = 'objItemCollection';
    const DATA_KEY_DEFAULT_REFERENTIAL_DATA = 'objReferentialData';
	
	
	
	// Exception message constants
    const EXCEPT_MSG_ITEM_COLLECTION_INVALID_FORMAT = 'Following item collection "%1$s" invalid! The collection must be an ItemCollection object, not null.';
	const EXCEPT_MSG_REFERENTIAL_DATA_FORMAT = 'Following referential "%1$s" invalid! The referential must be a ReferentialData object, not null.';



}