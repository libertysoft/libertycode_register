<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\library;



class ConstRegister
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Exception message constants
    const EXCEPT_MSG_KEY_INVALID_FORMAT = 'Following key "%1$s" invalid! The key must be a string, not empty.';
	const EXCEPT_MSG_KEY_FOUND = 'Following key "%1s" already exists!';
	const EXCEPT_MSG_KEY_NOT_FOUND = 'Following key "%1s" not found!';



}