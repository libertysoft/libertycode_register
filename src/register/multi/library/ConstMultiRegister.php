<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\multi\library;



class ConstMultiRegister
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_REGISTER = 'register';
    const TAB_CONFIG_KEY_NAME = 'name';
    const TAB_CONFIG_KEY_ORDER = 'order';

    // Execution configuration
    const TAB_EXEC_CONFIG_KEY_INCLUDE_NAME = 'include_name';
    const TAB_EXEC_CONFIG_KEY_EXCLUDE_NAME = 'exclude_name';
    const TAB_EXEC_CONFIG_KEY_REGISTER_EXECUTION_CONFIG = 'register_execution_config';


	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the multi register configuration standard.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the multi register execution configuration standard.';



}