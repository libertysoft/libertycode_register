<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\register\register\multi\model\MultiRegister;



// Init var
$objMemoryRegister = new MemoryRegister();
$objTableRegister = new DefaultTableRegister();
$tabConfigRegister = array(
    $objMemoryRegister,
    $objTableRegister
);
$tabConfig = array(
    'register' => [
        [
            'register' => $objMemoryRegister,
            'name' => 'mem'
        ],
        [
            'register' => $objTableRegister,
            'name' => 'tab',
            'order' => -1
        ]
    ]
);
$objRegister = new MultiRegister($tabConfig);



// Test add
$tabData = array(
    'key_1' => [
        'Value 1',
        [
            'exclude_name' => 'tab'
        ]
    ], // Ok
    'key_2' => [
        'Value 2',
        [
            'include_name' => 'tab' // Exclude mem
        ]
    ], // Ok
    'key_3' => [
        'Value 3',
        [
            'include_name' => ['tab', 'mem']
        ]
    ], // Ok
    'key_4' => [
        'Value 4',
        [
            'exclude_name' => ['tab']
        ]
    ], // Ok
    'key_5' => [
        'Value 5',
        [
            'exclude_name' => 'mem'
        ]
    ], // Ok
    'key_6' => [
        'Value 6',
        null
    ], // Ok
    '_key_1' => [
        'Value 1 duplicate',
        [
            'exclude_name' => 'tab'
        ]
    ], // Ko: key already exists
);

foreach($tabData as $strKey => $data)
{
    // Format key if required
    if(is_string($strKey) && (preg_match('#^_(.+)#', $strKey, $tabMatch) == 1))
    {
        $strKey = $tabMatch[1];
    }

    $value = $data[0];
    $tabConfig = $data[1];

    // Format key if required
    echo('Test add "'.$strKey.'": <br />');
    try{
        var_dump($objRegister->addItem($strKey, $value, $tabConfig));
        echo('Ok <br />');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test adding: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('Test adding (by register): <br />');
foreach($tabConfigRegister as $objConfigRegister)
{
    /** @var RegisterInterface $objConfigRegister */
    echo('Get register "'.get_class($objConfigRegister).'": <pre>');print_r($objConfigRegister->getTabItem());echo('</pre>');
}

echo('<br /><br /><br />');



// Test check/get
$tabKey = array(
	'key_1', // Found
	'key_2', // Found
	'key_3', // Found
	'key_4', // Found
	'key_5', // Found
	'key_6', // Found
	'key_7', // Not found
	'key_8', // Not found
);

foreach($tabKey as $strKey)
{
	echo('Test check, get "'.$strKey.'": <br />');
	try{
		echo('Exists: <pre>');var_dump($objRegister->checkItemExists($strKey));echo('</pre>');
		echo('Get: <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
	} catch(\Exception $e) {
		echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('<br /><br /><br />');



// Test set
$tabData = array(
    'key_1' => [
        'Value 1 update',
        [
            'exclude_name' => ['tab']
        ]
    ], // Ok
    'key_2' => [
        'Value 2 update',
        [
            'exclude_name' => ['mem']
        ]
    ], // Ok
    'key_3' => [
        'Value 3 update',
        null
    ], // Ok
    'key_4' => [
        'Value 4 update',
        null
    ], // Ok (Return false: not found on 'tab' register)
    'key_7' => [
        'Value 7',
        null
    ] // Ko: Not found
);

foreach($tabData as $strKey => $data)
{
    $value = $data[0];
    $tabConfig = $data[1];

	echo('Test set "'.$strKey.'": <br />');
	try{
		var_dump($objRegister->setItem($strKey, $value, $tabConfig));
        echo('Ok <br />');
	} catch(\Exception $e) {
		echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test setting: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test put
$tabData = array(
    'key_4' => [
        'Value 4 update 2',
        [
            'exclude_name' => ['tab']
        ]
    ], // Ok: update
    'key_7' => [
        'Value 7',
        null
    ] // Ok: create
);

foreach($tabData as $strKey => $data)
{
    $value = $data[0];
    $tabConfig = $data[1];

	echo('Test put "'.$strKey.'": <br />');
	try{
		var_dump($objRegister->putItem($strKey, $value, $tabConfig));
        echo('Ok <br />');
	} catch(\Exception $e) {
		echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test putting: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test remove
$tabKey = array(
	'key_1', // Ok
	'key_2', // Ok
	'key_3', // Ok
	'key_8' // Ko: not found
);

foreach($tabKey as $strKey)
{
	echo('Test remove "'.$strKey.'": <br />');
	try{
        var_dump($objRegister->removeItem($strKey));
        echo('Ok <br />');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test removing: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('Test removing (by register): <br />');
foreach($tabConfigRegister as $objConfigRegister)
{
    /** @var RegisterInterface $objConfigRegister */
    echo('Get register "'.get_class($objConfigRegister).'": <pre>');print_r($objConfigRegister->getTabItem());echo('</pre>');
}

echo('<br /><br /><br />');



// Test hydrate
$tabItem = array(
	'key_9' => 'Test 9', // Ok: create
	'key_10' => 10 // Ok: create
);
$objRegister->hydrateItem($tabItem);

echo('Test hydrate: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('Test hydrate (by register): <br />');
foreach($tabConfigRegister as $objConfigRegister)
{
    /** @var RegisterInterface $objConfigRegister */
    echo('Get register "'.get_class($objConfigRegister).'": <pre>');print_r($objConfigRegister->getTabItem());echo('</pre>');
}

echo('<br /><br /><br />');



// Test hydrate flush
$objRegister->hydrateItem();

echo('Test hydrate flush: <br />');
foreach($objRegister->getTabKey() as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('Test hydrate flush (by register): <br />');
foreach($tabConfigRegister as $objConfigRegister)
{
    /** @var RegisterInterface $objConfigRegister */
    echo('Get register "'.get_class($objConfigRegister).'": <pre>');print_r($objConfigRegister->getTabItem());echo('</pre>');
}

echo('<br /><br /><br />');


