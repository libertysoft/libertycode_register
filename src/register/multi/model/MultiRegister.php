<?php
/**
 * Description :
 * This class allows to define multi register class.
 * Multi register uses list of registers, as storage support.
 *
 * Multi register uses the following specified configuration:
 * [
 *     register(required): [
 *         // Register 1
 *         [
 *             register(required): object register interface 1,
 *
 *             name(optional: got register class name, if not found),
 *
 *             order(optional: 0 got if not found): integer
 *         ],
 *
 *         ...,
 *
 *         // Register N
 *         [...]
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\multi\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\register\register\api\RegisterInterface;

use liberty_code\register\register\multi\library\ConstMultiRegister;
use liberty_code\register\register\multi\exception\ConfigInvalidFormatException;
use liberty_code\register\register\multi\exception\ExecConfigInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 */
class MultiRegister extends FixBean implements RegisterInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig
     * @param array $tabItem = null
     */
    public function __construct(array $tabConfig, array $tabItem = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration
        $this->setTabConfig($tabConfig);

        // Hydrate items if needs
        if(!is_null($tabItem))
        {
            $this->hydrateItem($tabItem, false, true);
        }
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstMultiRegister::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstMultiRegister::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstMultiRegister::DATA_KEY_DEFAULT_CONFIG
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstMultiRegister::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of register configurations.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to update register configurations selection.
     * Null means no specific execution configuration required.
     * Format:
     * [
     *     include_name(optional: no name inclusion selection done, if not found):
     *         "string name,
     *         used to include register,
     *         which configuration name equal to following name"
     *         OR
     *         ["string name 1", ..., "string name N"],
     *
     *     exclude_name(optional: no name exclusion selection done, if not found):
     *         "string name,
     *         used to exclude register,
     *         which configuration name equal to following name"
     *         OR
     *         ["string name 1", ..., "string name N"]
     * ]
     *
     * @param array $tabConfig = null
     * @param null|boolean $sortAsc = null
     * @return array
     * @throws ExecConfigInvalidFormatException
     */
    protected function getTabRegisterConfig(array $tabConfig = null, $sortAsc = null)
    {
        // Check arguments
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $boolSortAsc = (is_bool($sortAsc) ? $sortAsc : null);
        $tabConfig = $this->getTabConfig();

        // Init register configurations
        $result = array_map(
            function($registerConfig){
                $result = $registerConfig;

                $result[ConstMultiRegister::TAB_CONFIG_KEY_NAME] = (
                    isset($registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_NAME]) ?
                        $registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_NAME] :
                        get_class($registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_REGISTER])
                );

                return $result;
            },
            array_values($tabConfig[ConstMultiRegister::TAB_CONFIG_KEY_REGISTER])
        );

        // Filter register configurations, if required
        $result = array_filter(
            $result,
            function($registerConfig) use ($tabConfig, $tabExecConfig) {
                return (
                    // Filter per included name, if required
                    (
                        (!isset($tabExecConfig[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_INCLUDE_NAME])) ||
                        (
                            is_array($tabExecConfig[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_INCLUDE_NAME]) &&
                            in_array(
                                $registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_NAME],
                                $tabExecConfig[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_INCLUDE_NAME]
                            )
                        ) ||
                        (
                            is_string($tabExecConfig[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_INCLUDE_NAME]) &&
                            ($registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_NAME] ==
                                $tabExecConfig[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_INCLUDE_NAME])
                        )
                    ) &&

                    // Filter per excluded name, if required
                    (
                        (!isset($tabExecConfig[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_EXCLUDE_NAME])) ||
                        (
                            is_array($tabExecConfig[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_EXCLUDE_NAME]) &&
                            (!in_array(
                                $registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_NAME],
                                $tabExecConfig[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_EXCLUDE_NAME]
                            ))
                        ) ||
                        (
                            is_string($tabExecConfig[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_EXCLUDE_NAME]) &&
                            ($registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_NAME] !=
                                $tabExecConfig[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_EXCLUDE_NAME])
                        )
                    )
                );
            }
        );

        // Sort register configurations, if required
        if(is_bool($boolSortAsc))
        {
            usort(
                $result,
                function($registerConfig1, $registerConfig2)
                {
                    $intOrder1 = (
                        array_key_exists(ConstMultiRegister::TAB_CONFIG_KEY_ORDER, $registerConfig1) ?
                            $registerConfig1[ConstMultiRegister::TAB_CONFIG_KEY_ORDER] :
                            0
                    );
                    $intOrder2 = (
                        array_key_exists(ConstMultiRegister::TAB_CONFIG_KEY_ORDER, $registerConfig2) ?
                            $registerConfig2[ConstMultiRegister::TAB_CONFIG_KEY_ORDER] :
                            0
                    );
                    $result = (($intOrder1 < $intOrder2) ? -1 : (($intOrder1 > $intOrder2) ? 1 : 0));

                    return $result;
                }
            );

            // Get descending sort, if required
            if(!$boolSortAsc)
            {
                krsort($result);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get index array of registers.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to update registers selection.
     * Null means no specific execution configuration required.
     * Format:
     * [
     *     @see getTabRegisterConfig() configuration array format,
     *
     *     register_execution_config(optional: got [], if not found):[
     *         // Register execution configuration 1
     *         "string name 1" (name used on @see MultiRegister configuration array format, on register key): [
     *             register execution configuration array format
     *         ],
     *
     *         ...,
     *
     *         // Register execution configuration N
     *         "string name N": [...]
     *     ]
     * ]
     *
     * Register execution configurations array format:
     * Index array of register execution configurations can be returned,
     * where index corresponds with returned array index.
     *
     * @param array $tabConfig = null
     * @param array &$tabRegisterExecConfig = array()
     * @return array
     * @throws ExecConfigInvalidFormatException
     */
    protected function getTabRegister(array $tabConfig = null, array &$tabRegisterExecConfig = array())
    {
        // Init var
        $result = array();
        $tabRegisterExecConfig = array();
        $tabRegisterConfig = $this->getTabRegisterConfig($tabConfig, true);

        // Run each register config
        foreach($tabRegisterConfig as $registerConfig)
        {
            // Get info
            /** @var RegisterInterface $objRegister */
            $objRegister = $registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_REGISTER];
            $strNm = $registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_NAME];
            $tabExecConfig = (
                isset($tabConfig[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_REGISTER_EXECUTION_CONFIG][$strNm]) ?
                    $tabConfig[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_REGISTER_EXECUTION_CONFIG][$strNm] :
                    null
            );

            // Set result
            $result[] = $objRegister;
            $tabRegisterExecConfig[] = $tabExecConfig;
        }

        // Return result
        return $result;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Execute registers.
     *
     * Execute register callback format:
     * mixed function(
     *     RegisterInterface $objRegister,
     *     array $tabExecConfig = null,
     *     boolean &$boolContinue = true,
     *     mixed $previousResult
     * ):
     * Execute specified register.
     *
     * Configuration array format:
     * Execution configuration can be provided:
     * @see getTabRegister() configuration array format.
     *
     * @param callable $callableRegister
     * @param array $tabConfig = null
     * @return null|mixed
     * @throws ExecConfigInvalidFormatException
     */
    protected function executeRegister(callable $callableRegister, array $tabConfig = null)
    {
        // Init var
        $result = null;
        $tabRegisterExecConfig = array();
        $tabRegister = $this->getTabRegister($tabConfig, $tabRegisterExecConfig);

        // Run each register
        $boolContinue = true;
        for($intCpt = 0; ($intCpt < count($tabRegister)) && $boolContinue; $intCpt++)
        {
            // Get info
            /** @var RegisterInterface $objRegister */
            $objRegister = $tabRegister[$intCpt];
            $tabExecConfig = $tabRegisterExecConfig[$intCpt];

            // Call register action
            $result = $callableRegister($objRegister, $tabExecConfig, $boolContinue, $result);
        }

        // Return result
        return $result;
    }





    // Methods initialize (Register interface implementation)
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function hydrateItem(array $tabItem = array(), $boolOverwrite = false, $boolClear = true)
    {
        // Init var
        $result = $this->executeRegister(
            function(RegisterInterface $objRegister, $tabExecConfig, &$boolContinue, $previousResult)
            use($tabItem, $boolOverwrite, $boolClear)
            {
                // Init var
                $result =
                    $objRegister->hydrateItem($tabItem, $boolOverwrite, $boolClear) &&
                    (is_bool($previousResult) ? $previousResult : true);
                $boolContinue = true;

                // Return result
                return $result;
            }
        );

        // Return result
        return $result;
    }





    // Methods check (Register interface implementation)
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkItemExists($strKey)
    {
        // Init var
        $result = $this->executeRegister(
            function(RegisterInterface $objRegister, $tabExecConfig, &$boolContinue, $previousResult)
            use($strKey)
            {
                // Init var
                $result = $objRegister->checkItemExists($strKey);
                $boolContinue = (!$result);

                // Return result
                return $result;
            }
        );

        // Return result
        return $result;
    }





    // Methods getters (Register interface implementation)
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getItem($strKey)
    {
        // Init var
        $result = $this->executeRegister(
            function(RegisterInterface $objRegister, $tabExecConfig, &$boolContinue, $previousResult)
            use($strKey)
            {
                // Init var
                $result = $objRegister->getItem($strKey);
                $boolContinue = (is_null($result));

                // Return result
                return $result;
            }
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided:
     * @see executeRegister() configuration array format,
     * where each register execution configuration array
     * follows @see RegisterInterface::getTabKey() configuration array format.
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     */
    public function getTabKey(array $tabConfig = null)
    {
        // Init var
        $result = $this->executeRegister(
            function(RegisterInterface $objRegister, $tabExecConfig, &$boolContinue, $previousResult)
            {
                // Init var
                $result = (is_array($previousResult) ? $previousResult : array());
                $tabKey = $objRegister->getTabKey($tabExecConfig);
                $boolContinue = true;

                // Run each item
                foreach($tabKey as $strKey)
                {
                    // Set item, if required
                    if(!in_array($strKey, $result))
                    {
                        $result[] = $strKey;
                    }
                }

                // Return result
                return $result;
            },
            $tabConfig
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided:
     * @see executeRegister() configuration array format,
     * where each register execution configuration array
     * follows @see RegisterInterface::getTabItem() configuration array format.
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     */
    public function getTabItem(array $tabConfig = null)
    {
        // Init var
        $result = $this->executeRegister(
            function(RegisterInterface $objRegister, $tabExecConfig, &$boolContinue, $previousResult)
            {
                // Init var
                $result = (is_array($previousResult) ? $previousResult : array());
                $tabItem = $objRegister->getTabItem($tabExecConfig);
                $boolContinue = true;

                // Run each item
                foreach($tabItem as $itemKey => $item)
                {
                    // Set item, if required
                    if(!array_key_exists($itemKey, $result))
                    {
                        $result[$itemKey] = $item;
                    }
                }

                // Return result
                return $result;
            },
            $tabConfig
        );

        // Return result
        return $result;
    }





    // Methods setters (Register interface implementation)
    // ******************************************************************************

    /**
     * Configuration array format:
     * Execution configuration can be provided:
     * @see executeRegister() configuration array format,
     * where each register execution configuration array
     * follows @see RegisterInterface::addItem() configuration array format.
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     */
    public function addItem($strKey, $item, array $tabConfig = null)
    {
        // Init var
        $result = $this->executeRegister(
            function(RegisterInterface $objRegister, $tabExecConfig, &$boolContinue, $previousResult)
            use($strKey, $item)
            {
                // Init var
                $result =
                    (!$objRegister->checkItemExists($strKey)) &&
                    $objRegister->addItem($strKey, $item, $tabExecConfig) &&
                    (is_bool($previousResult) ? $previousResult : true);
                $boolContinue = true;

                // Return result
                return $result;
            },
            $tabConfig
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided:
     * @see executeRegister() configuration array format,
     * where each register execution configuration array
     * follows @see RegisterInterface::setItem() configuration array format.
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     */
    public function setItem($strKey, $item, array $tabConfig = null)
    {
        // Init var
        $result = $this->executeRegister(
            function(RegisterInterface $objRegister, $tabExecConfig, &$boolContinue, $previousResult)
            use($strKey, $item)
            {
                // Init var
                $result =
                    $objRegister->checkItemExists($strKey) &&
                    $objRegister->setItem($strKey, $item, $tabExecConfig) &&
                    (is_bool($previousResult) ? $previousResult : true);
                $boolContinue = true;

                // Return result
                return $result;
            },
            $tabConfig
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided:
     * @see executeRegister() configuration array format,
     * where each register execution configuration array
     * follows @see RegisterInterface::putItem() configuration array format.
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     */
    public function putItem($strKey, $item, array $tabConfig = null)
    {
        // Init var
        $result = $this->executeRegister(
            function(RegisterInterface $objRegister, $tabExecConfig, &$boolContinue, $previousResult)
            use($strKey, $item)
            {
                // Init var
                $result =
                    $objRegister->putItem($strKey, $item, $tabExecConfig) &&
                    (is_bool($previousResult) ? $previousResult : true);
                $boolContinue = true;

                // Return result
                return $result;
            },
            $tabConfig
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeItem($strKey)
    {
        // Init var
        $result = $this->executeRegister(
            function(RegisterInterface $objRegister, $tabExecConfig, &$boolContinue, $previousResult)
            use($strKey)
            {
                // Init var
                $result =
                    (
                        $objRegister->checkItemExists($strKey) &&
                        $objRegister->removeItem($strKey)
                    ) ||
                    (is_bool($previousResult) ? $previousResult : false);
                $boolContinue = true;

                // Return result
                return $result;
            }
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided:
     * @see executeRegister() configuration array format,
     * where each register execution configuration array
     * follows @see RegisterInterface::removeItemAll() configuration array format.
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     */
    public function removeItemAll(array $tabConfig = null)
    {
        // Init var
        $result = $this->executeRegister(
            function(RegisterInterface $objRegister, $tabExecConfig, &$boolContinue, $previousResult)
            {
                // Init var
                $result =
                    $objRegister->removeItemAll($tabExecConfig) &&
                    (is_bool($previousResult) ? $previousResult : true);
                $boolContinue = true;

                // Return result
                return $result;
            },
            $tabConfig
        );

        // Return result
        return $result;
    }



}