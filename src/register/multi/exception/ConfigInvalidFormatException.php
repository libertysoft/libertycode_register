<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\multi\exception;

use Exception;

use liberty_code\register\register\api\RegisterInterface;
use liberty_code\register\register\multi\library\ConstMultiRegister;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMultiRegister::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init register configuration array check function
        $checkTabRegisterConfigIsValid = function($tabRegisterConfig)
        {
            $result = is_array($tabRegisterConfig) && (count($tabRegisterConfig) > 0);

            // Check each register configuration valid, if required
            if($result)
            {
                $tabRegisterConfig = array_values($tabRegisterConfig);
                for($intCpt = 0; ($intCpt < count($tabRegisterConfig)) && $result; $intCpt++)
                {
                    $registerConfig = $tabRegisterConfig[$intCpt];
                    $result = (
                        // Check valid register config
                        is_array($registerConfig) &&

                        // Check valid register
                        isset($registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_REGISTER]) &&
                        ($registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_REGISTER] instanceof RegisterInterface) &&

                        // Check valid name
                        (
                            (!isset($registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_NAME])) ||
                            (
                                is_string($registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_NAME]) &&
                                (trim($registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_NAME]) != '')
                            )
                        ) &&

                        // Check valid order
                        (
                            (!isset($registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_ORDER])) ||
                            is_int($registerConfig[ConstMultiRegister::TAB_CONFIG_KEY_ORDER])
                        )
                    );
                }
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid register
            isset($config[ConstMultiRegister::TAB_CONFIG_KEY_REGISTER]) &&
            $checkTabRegisterConfigIsValid($config[ConstMultiRegister::TAB_CONFIG_KEY_REGISTER]);

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}