<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\register\multi\exception;

use Exception;

use liberty_code\register\register\multi\library\ConstMultiRegister;



class ExecConfigInvalidFormatException extends Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstMultiRegister::EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function(array $tabStr)
        {
            $result = true;
            $tabStr = array_values($tabStr);

            // Check each value is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    // Check valid string
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init register execution configuration array check function
        $checkTabRegisterExecConfigIsValid = function($tabRegisterExecConfig) use ($checkTabStrIsValid)
        {
            $result = is_array($tabRegisterExecConfig);

            // Check each register execution configuration valid, if required
            if($result)
            {
                // Init var
                $tabNm = array_keys($tabRegisterExecConfig);

                // Check each register name and associated register execution config valid
                for($intCpt = 0; ($intCpt < count($tabNm)) && $result; $intCpt++)
                {
                    $strNm = $tabNm[$intCpt];
                    $tabExecConfig = $tabRegisterExecConfig[$strNm];
                    $result = (
                        // Check valid name
                        is_string($strNm) && (trim($strNm) != '') &&

                        // Check valid execution configuration
                        is_array($tabExecConfig)
                    );
                }
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid include name
            (
                (!isset($config[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_INCLUDE_NAME])) ||
                (
                    is_string($config[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_INCLUDE_NAME]) &&
                    (trim($config[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_INCLUDE_NAME]) != '')
                ) ||
                (
                    is_array($config[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_INCLUDE_NAME]) &&
                    $checkTabStrIsValid($config[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_INCLUDE_NAME])
                )
            ) &&

            // Check valid exclude name
            (
                (!isset($config[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_EXCLUDE_NAME])) ||
                (
                    is_string($config[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_EXCLUDE_NAME]) &&
                    (trim($config[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_EXCLUDE_NAME]) != '')
                ) ||
                (
                    is_array($config[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_EXCLUDE_NAME]) &&
                    $checkTabStrIsValid($config[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_EXCLUDE_NAME])
                )
            ) &&

            // Check valid register
            (
                (!isset($config[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_REGISTER_EXECUTION_CONFIG])) ||
                $checkTabRegisterExecConfigIsValid($config[ConstMultiRegister::TAB_EXEC_CONFIG_KEY_REGISTER_EXECUTION_CONFIG])
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}