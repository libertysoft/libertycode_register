<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\referential\exception;

use liberty_code\register\referential\library\ConstReferential;



class HashInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $hash
     */
	public function __construct($hash) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstReferential::EXCEPT_MSG_HASH_INVALID_FORMAT, strval($hash));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified hash has valid format
	 * 
     * @param mixed $hash
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($hash)
    {
		// Init var
		$result = (is_string($hash) && (trim($hash) != ''));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($hash) ? serialize($hash) : $hash));
		}
		
		// Return result
		return $result;
    }
	
	
	
}