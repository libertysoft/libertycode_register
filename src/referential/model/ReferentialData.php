<?php
/**
 * Description :
 * This class allows to define referential class.
 * item key => item hash.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\referential\model;

use liberty_code\data\data\table\model\TableData;

use liberty_code\register\referential\exception\HashInvalidFormatException;



class ReferentialData extends TableData
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function checkValidValue($strPath, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			HashInvalidFormatException::setCheck($value);
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





	// Methods check
	// ******************************************************************************
	
	/**
	 * Check if the specified hash exists.
	 * 
	 * @param string $strHash
	 * @return boolean
	 */
	public function checkHashExists($strHash)
	{
		// Init var
        $tabDataSrc = $this->getUseTabDataSrc();
		$result = in_array($strHash, $tabDataSrc);
		
		// Return result
		return $result;
	}
	
	
	
}