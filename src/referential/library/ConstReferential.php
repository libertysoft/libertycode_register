<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\register\referential\library;



class ConstReferential
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Exception message constants
	const EXCEPT_MSG_HASH_INVALID_FORMAT = 'Following hash "%1$s" invalid! The hash must be a string, not empty.';
}