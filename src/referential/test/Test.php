<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\register\referential\model\ReferentialData;



// Init var
$objReferential = new ReferentialData();



// Test add data
echo('Test add data: <br />');

$tabHash = array(
	['Key 1', 'Hash 1'],
	['Key 2', 'Hash 2'],
	['Key 3', 'Hash 3'],
	['Key 4', 'Hash 4'],
	['Key 1', 'Hash 5'],
	['Key 6', true],
	['Key 7', 1],
	['Key 8', 2.2],
	['Key 9', []],
	['Key 10', null],
	['Key 11', 'Hash 11']
);

$tabKey = array();
foreach($tabHash as $strKey => $tabInfoHash)
{
	try{
		$strKey = $tabInfoHash[0];
		$hash = $tabInfoHash[1];
		$objReferential->addValue($strKey, $hash);
		$tabKey[] = $strKey;
		
		echo('Key: <pre>');var_dump($strKey);echo('</pre>');
		echo('Value: <pre>');var_dump($hash);echo('</pre>');
		echo('Value from referential: <pre>');var_dump($objReferential->getValue($strKey));echo('</pre>');
		echo('<br />');
	} catch(\Exception $e) {
		echo($e->getMessage());
		echo('<br />');
	}
}

echo('<br /><br /><br />');



// Test run data
echo('Test run data: <br />');

$intCpt = 0;
foreach(array_keys($objReferential->getDataSrc()) as $strKey)
{
	echo('Key: <pre>');var_dump($strKey);echo('</pre>');
	echo('Key from add: <pre>');var_dump($tabKey[$intCpt]);echo('</pre>');
	echo('Value: <pre>');print_r($objReferential->getValue($strKey));echo('</pre>');
	echo('<br />');
	
	$intCpt++;
}

echo('<br /><br /><br />');


