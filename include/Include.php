<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/item/library/ConstItem.php');
include($strRootPath . '/src/item/exception/ItemInvalidFormatException.php');
include($strRootPath . '/src/item/model/ItemCollection.php');

include($strRootPath . '/src/item/instance/library/ConstInstance.php');
include($strRootPath . '/src/item/instance/exception/InstanceInvalidFormatException.php');
include($strRootPath . '/src/item/instance/model/InstanceCollection.php');

include($strRootPath . '/src/referential/library/ConstReferential.php');
include($strRootPath . '/src/referential/exception/HashInvalidFormatException.php');
include($strRootPath . '/src/referential/model/ReferentialData.php');

include($strRootPath . '/src/register/library/ConstRegister.php');
include($strRootPath . '/src/register/exception/KeyInvalidFormatException.php');
include($strRootPath . '/src/register/exception/KeyFoundException.php');
include($strRootPath . '/src/register/exception/KeyNotFoundException.php');
include($strRootPath . '/src/register/api/RegisterInterface.php');
include($strRootPath . '/src/register/model/DefaultRegister.php');

include($strRootPath . '/src/register/memory/library/ConstMemoryRegister.php');
include($strRootPath . '/src/register/memory/exception/ItemCollectionInvalidFormatException.php');
include($strRootPath . '/src/register/memory/exception/ReferentialDataInvalidFormatException.php');
include($strRootPath . '/src/register/memory/model/MemoryRegister.php');

include($strRootPath . '/src/register/table/library/ConstTableRegister.php');
include($strRootPath . '/src/register/table/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/register/table/exception/GetExecConfigInvalidFormatException.php');
include($strRootPath . '/src/register/table/exception/SetExecConfigInvalidFormatException.php');
include($strRootPath . '/src/register/table/model/TableRegister.php');
include($strRootPath . '/src/register/table/model/DefaultTableRegister.php');

include($strRootPath . '/src/register/global_var/library/ConstGlobalVarRegister.php');
include($strRootPath . '/src/register/global_var/model/GlobalVarRegister.php');

include($strRootPath . '/src/register/multi/library/ConstMultiRegister.php');
include($strRootPath . '/src/register/multi/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/register/multi/exception/ExecConfigInvalidFormatException.php');
include($strRootPath . '/src/register/multi/model/MultiRegister.php');